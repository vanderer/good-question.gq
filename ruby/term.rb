class Term
  #    "a" -> {a, nil,nil}
  #
  #    "+","a","b" -> {+, {a,nil,nil}, {b,nil,nil}}
  #
  #    {+,{a,nil,nil},{b,nil,nil}}
  def initialize(op,left=nil,right=nil)
    if left.class==Term
      @left = left
    elsif left
      @left = Term.new(left)
    end
    if right.class==Term
      @right=right
    elsif right
      @right = Term.new(right)
    end
    @head = op
  end

  def left
    return @left
  end

  def right
    return @right
  end

  def head
    return @head
  end

  def inspect(level=0)
    praefix = " "*level
    answer = ""
    answer += praefix+@head.to_s
    answer += "\n"
    if @left==nil
      answer += praefix+praefix+@left.inspect
    else
      answer += praefix+@left.inspect(level+1)
    end
    answer += "\n"
    if @right==nil
      answer += praefix+praefix+@right.inspect
    else
      answer += praefix+@right.inspect(level+1)
    end
    return answer
  end

  def to_s
    if @left
      if @head=="*"
        return "#{@left.to_s} \\cdot #{@right.to_s}"
      elsif @head=="/"
        return "\\frac{#{@left.to_s} }{ #{@right.to_s}}"
      else
        return "(#{@left.to_s} #{@head.to_s} #{@right.to_s})"
      end
    else
      return "#{@left.to_s} #{@head.to_s} #{@right.to_s}"
    end
  end

  def contains?(var)
    if @left!=nil and @right!=nil
      return (@head==var or @right.contains?(var) or @left.contains?(var))
    else
      return @head==var
    end
  end
end
