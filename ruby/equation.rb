class Equation
  def initialize(left,right)
    @left=left
    @right=right
  end

  def to_s
    return "#{@left.to_s} = #{@right.to_s}"
  end

  def to_tex
    return "#{@left.to_s} &= #{@right.to_s}\\\\"
  end

  def solve_step(var)
    if @left.contains?(var) and not @right.contains?(var)
      #extract var from left side
      case @left.head
      when "+" then
        if @left.left.contains?(var)
          puts "&\\text{Subtraktion von }#{@left.right.to_s}\\\\"
          @right = Term.new("-", @right, @left.right)
          @left = @left.left
        else
          puts "&\\text{Subtraktion von }#{@left.left.to_s}\\\\"
          @right = Term.new("-", @right, @left.left)
          @left = @left.right
        end
      when "-" then
        puts "&\\text{Addition von }#{@left.right.to_s}\\\\"
        @right = Term.new("+", @right, @left.right)
        @left = @left.left
      when "*" then
        if @left.left.contains?(var)
          puts "&\\text{Division durch }#{@left.right.to_s}\\\\"
          @right = Term.new("/", @right, @left.right)
          @left = @left.left
        else
          puts "&\\text{Division durch }#{@left.left.to_s}\\\\"
          @right = Term.new("/", @right, @left.left)
          @left = @left.right
        end
      when "/" then
        puts "&\\text{Multiplikation mit }#{@left.right.to_s}\\\\"
        @right = Term.new("*", @right, @left.right)
        @left = @left.left
      end
    elsif @right.contains?(var) and not @left.contains?(var)
      puts "&\\text{Vertauschen der rechten und linken Seite}\\\\"
      temp_right = @right
      temp_left = @left
      @left = temp_right
      @right = temp_left
      #extrac var fram left side yet again!
    else
      #throw the var to the left side as soon as it is @left or @right of a Term
    end
  end

  def left
    return @left
  end

  def right
    return @right
  end
end
