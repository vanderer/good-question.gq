require "/home/csabi/htdocs/ruby/term.rb"
require "/home/csabi/htdocs/ruby/equation.rb"

vars=["a","b","c"]
nums=["2","5","8","12"]
ops=["+","-","*","/"]

a = Term.new(ops.sample, nums.sample+vars.sample, vars.sample)
b = Term.new(ops.sample, nums.sample+vars.sample, vars.sample)
c = Term.new(ops.sample, nums.sample+vars.sample, b)
#c = Term.new("*", "d",b)



v=vars.sample
e = Equation.new(a, c)

puts "\\documentclass{scrartcl}
\\usepackage{amsmath}
\\usepackage[utf8]{inputenc}
\\begin{document}

\\section{Löse die Gleichung nach #{v}!}
"
puts "\\begin{align*}"
while e.left.to_s != (" "+v+" ")
  puts e.to_tex
  e.solve_step(v)
end
puts e.to_tex
puts "\\end{align*}"
puts "\\end{document}"


#puts e.left.contains?(v)
#puts e.right.inspect
#puts e.right.contains?(v)
#puts c.inspect
#puts c.contains?(v)
