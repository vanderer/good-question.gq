<!DOCTYPE html>
<?php extract($_GET); ?>
<html lang="en">
<head>
  <title>Good Question<?php if (isset($section)){echo " - "; include "sections/$section/title";}?></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="http://good-question.gq/css/style.css">
  <!--link rel="stylesheet" href="http://good-question.gq/css/bootstrap.min.css"-->
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.0/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <div class="row content">
    <div class="col-sm-3 sidenav">
      <h1><a id='logo' href="/">Good Question</a></h1>
      <div class="input-group">
        <input type="text" class="form-control" placeholder="Diese Suche tut nichts.">
        <span class="input-group-btn">
          <button class="btn btn-default" type="button">
            <!--span class="glyphicon glyphicon-search"></span-->
            suchen
          </button>
        </span>
      </div><br>
      <ul class="nav nav-pills nav-stacked">
<?php
$kapitel=[];
if ($handle = opendir('sections')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
          array_push($kapitel,$entry);
        }
    }
    closedir($handle);
}
sort($kapitel);
while (list($key, $entry) = each($kapitel)) {
  if ($entry==$section){
    echo "<li class=\"nav section\"><a class=section href=\"?section=$entry\">";
  } else {
    echo "<li class=\"nav\"><a href=\"?section=$entry\">";
  }
  include("sections/$entry/title");
  echo "</a></li>";
}
?>
      </ul>
    </div>

    <div id="content" class="col-sm-9 section">
      <?php
        if (isset($section)){
          echo "<h1>";
          include "sections/$section/title";
          echo "</h1>";
          include "sections/$section/main.php";
        } else {
          echo "<h3>Letzte Updates</h3>";
          echo shell_exec("git log --pretty=format:\"<hr/><h4>%an | %ad</h4><p class=changelog>%s</p>\" | sed -e 's/[+-]....//'");
        }
      ?>
    </div>

<footer class="container-fluid">
  <p>Der Zweck dieser Seite ist Übungsmaterial für verschiedene Schulfächer bereitzustellen. </p>
</footer>

</body>
</html>
