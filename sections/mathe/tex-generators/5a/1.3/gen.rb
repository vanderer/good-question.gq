#!/usr/bin/env ruby
require("/home/csabi/.csonfig/Resources/ProgLib/latexfuncions.rb")

n=10

texHeader("beamer")
puts "\\begin{frame}"
tablearray = []

n.times do
  r=rand(15).to_f+1
  h=rand(25).to_f+1

  m=Math.sqrt(h**2+r**2).round(2)
  v=(r**2*h*3.1415926535/3).round(2)
  rowarray = [r,h,m,v]
  tablearray << rowarray
end

gaps=[]
tablearray.each do |line|
  x=rand(line.length)
  y=rand(line.length)
  while y==x do
    y=rand(line.length)
  end
  gaps << [x,y]
end

#insert header line for tables with no stamp-out
titelzeile = [ "\\textbf{r}", "\\textbf{h}", "\\textbf{m}", "\\textbf{V}" ]
tablearray.insert(0,titelzeile)
gaps.insert(0,[])

#generate the latex code for the table lines
loesung = twoDimTab(tablearray)
aufgabe = twoDimTab(twoDimStampOut(tablearray,gaps))

puts "\\frametitle{Aufgabe - Berechnungen am Kegel}"
puts "Berechne die Fehlenden Werte"
puts aufgabe
puts "\\end{frame}"
puts "\\begin{frame}"
puts "\\frametitle{Lösung}"
puts loesung

puts "\\end{frame}"

puts "\\end{document}"
