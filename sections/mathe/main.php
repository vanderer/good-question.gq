<?php
echo "<ul>";
$kapitel=[];
if ($handle = opendir('sections/mathe/kapitel')) {
    while (false !== ($entry = readdir($handle))) {
        if ($entry != "." && $entry != "..") {
          array_push($kapitel,$entry);
        }
    }
    closedir($handle);
}
sort($kapitel);
while (list($key, $entry) = each($kapitel)) {
   //echo "$entry\n";
  if ($entry==$subsection){
    echo "<li class=\"nav section inline\"><a class='subsection active' href=\"?section=mathe&subsection=$entry\">";
  } else {
    echo "<li class=\"nav section inline\"><a class=section href=\"?section=mathe&subsection=$entry\">";
  }
  echo "$entry";
  echo "</a></li>";
}
echo "</ul>";
?>
<div class="flex-grow subsection">
<?php
  if (isset($subsection)){
    echo "<h2>";
    echo "$subsection";
    echo "</h2>";
    $dateien=[];
    if ($handle = opendir("sections/mathe/kapitel/$subsection")) {
        while (false !== ($entry = readdir($handle))) {
            if ($entry != "." && $entry != "..") {
              array_push($dateien,$entry);
            }
        }
        closedir($handle);
    }
    sort($dateien);
    while (list($key, $entry) = each($dateien)) {
      echo "<li class=\"nav subsection\"><a class=\"pdffile\" href=\"sections/mathe/kapitel/$subsection/$entry\">";
      echo "$entry";
      echo "</a></li>";
    }
    include("sections/mathe/kapitelsites/$subsection.php");
  }
?>
</div>
